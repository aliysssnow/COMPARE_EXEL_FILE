VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Sheet1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True
''Rice pure and white clean the fields of delight.

Sub RiceAtValues()
    Range("A5:SA5124").ClearContents
    Range("A5:SA5124").ClearFormats
End Sub
Sub STORCH()

''CradleName: Name of the sourcefile for importing values ergo the Cradle.
''BlockName: Name of the sourcefile for exporting values ergo the Block.
Dim CradleName As String
Dim BlockName As String

''Takes Value of CradleName from Cell "A5"
''Takes Value of BlockName from Cell "A6"
CradleName = Worksheets("OCIAN").Range("A5").Value
BlockName = Worksheets("OCIAN").Range("A6").Value

Dim Nametag As String
Nametag = InputBox("Please insert a value of a column to compare with the second file. The value has to be case-correct and exactly the same as in the second file.", "MASTER_FILE_scripted_by_ALIYSS")
Dim Address As String
Address = InputBox("Please insert a value of a column to insert within the first file. The value has to be case-correct and exactly the same as in the second file. If there are multiple values be sure to seperate them with commas.", "MASTER_FILE_scripted_by_ALIYSS")
ScytheValues = Split(Address, ", ")

''Minimizes MASTER_FILE_scripted_by_ALIYSS
ActiveWorkbook.Windows(1).WindowState = xlMinimized

''ListOfPetrus: File containing information of the Cradle.
Dim ListOfPetrus As Excel.Workbook
Set ListOfPetrus = Workbooks.Open(CradleName)
ListOfPetrus.Windows(1).WindowState = xlMinimized

''ListOfPetrus: File containing information of the Block.
Dim ListOfHatter As Excel.Workbook
Set ListOfHatter = Workbooks.Open(BlockName)
ListOfHatter.Windows(1).WindowState = xlMinimized

''lastbaby: Searches for the Cell with the word "Nametag" from Cells "A1" to "ZZ1" in List Of Petrus.
''lasthouse: Searches for the Cell with the word "Nametag" from Cells "A1" to "ZZ1" in List Of Hatter.
lastbaby = ListOfPetrus.Worksheets(1).Cells(ListOfPetrus.Worksheets(1).Rows.Count, ListOfPetrus.Worksheets(1).Range("A1:ZZ1").Find(Nametag, LookIn:=xlValues, lookat:=xlWhole).Column).End(xlUp).Row
lasthouse = ListOfHatter.Worksheets(1).Cells(ListOfHatter.Worksheets(1).Rows.Count, ListOfHatter.Worksheets(1).Range("A1:ZZ1").Find(Nametag, LookIn:=xlValues, lookat:=xlWhole).Column).End(xlUp).Row



''LookingYoung: Searches the Column with for the word "ID" from Cell "A1" to "ZZ1" in the List Of Petrus.
''LookingOld: Searches the Column with for the word "ID" from Cell "A1" to "ZZ1" in List Of Hatter.
LookingYoung = ListOfPetrus.Worksheets(1).Range("A1:ZZ1").Find(Nametag, LookIn:=xlValues, lookat:=xlWhole).Column
LookingOld = ListOfHatter.Worksheets(1).Range("A1:ZZ1").Find(Nametag, LookIn:=xlValues, lookat:=xlWhole).Column

''firstbaby: Startvalue for counting of Row 2 in the List Of Petrus downwards.
''firsthouse: Startvalue for counting of Row 2 in the List Of Hatter downwards.
firstbaby = 2
firsthouse = 2


''Storch takes the bluebaby also known as the lastbaby and takes it to the bluehouse in this case the lasthouse.
''If the lasthouse is as blue as the baby it gets assigned to the bluebaby.
''bluebaby gets a warm home and is now no more a bluebaby.
''Storch flies back and gets the next bluebaby.

For bluebaby = lastbaby To firstbaby Step -1

''bonebaby is an individual value which has been assigned to every baby.
''If bonebaby is equal to bonehouse Storch stays put and gets the appel and the bones of the house.
''If not, then Storch keeps flying.

bonebaby = ListOfPetrus.Worksheets(1).Cells(bluebaby, LookingYoung).Value

    For bluehouse = lasthouse To firsthouse Step -1
        
        bonehouse = ListOfHatter.Worksheets(1).Cells(bluehouse, LookingOld).Value

        If bonebaby = bonehouse Then
            
            For Each Grass In ScytheValues
            
                ''LookingMinute: Searches for the Column with the word "Grass" from Cell "A1" to "ZZ1" in List Of Petrus.
                ''LookingSecond: Searches for the Column with the word "Grass" from Cell "A1" to "ZZ1" in List Of Hatter.
                LookingMinute = ListOfPetrus.Worksheets(1).Range("A1:ZZ1").Find(Grass, LookIn:=xlValues, lookat:=xlWhole).Column
                LookingSecond = ListOfHatter.Worksheets(1).Range("A1:ZZ1").Find(Grass, LookIn:=xlValues, lookat:=xlWhole).Column
            
                appel = ListOfHatter.Worksheets(1).Cells(bluehouse, LookingSecond).Value
                ListOfPetrus.Worksheets(1).Cells(bluebaby, LookingMinute).Value = appel
            Next Grass
        End If

    Next bluehouse
Next bluebaby

''If Storch is tired, then Storch will die.
''If Storch is finished, then Storch will display the list of babies and their bones and appels in their Cradle.

ListOfPetrus.Close savechanges:=True
ListOfHatter.Close savechanges:=False
Set ListOfPetrus = Workbooks.Open(CradleName)
ListOfPetrus.Activate
ListOfPetrus.Windows(1).WindowState = xlMaximized
End Sub
